import csv

from django.contrib import messages
from django.shortcuts import render, redirect
from django.core.files.storage import FileSystemStorage
from django.views.generic import TemplateView
from .forms import FileForm, DataToBeStoredForm
from rest_framework import viewsets
from.serializers import FileSerializer, DataToBeStoredSerializer
import io

from .models import File,DataToBeStored
from rest_framework.filters import SearchFilter, OrderingFilter
from .pagination import PostLimitOffPagination,PostPageNumberPagination

class Home(TemplateView):
    template_name = 'index.html'

# def upload(request):
#     if request.method == 'POST':
#         doc=request.FILES
#         uploaded_file=doc['document']
#         fs=FileSystemStorage()
#         fs.save(uploaded_file.name, uploaded_file)
#     return render(request, 'upload.html')



def show_file(request):
    files= File.objects.all()
    return render(request, 'files_list.html', {"files":files})


def upload_file(request):
    if request.method=='POST':
        # form =DataToBeStoredForm(request.POST)
        csv_file=request.FILES['file']

        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'this is not a csv file')

        data_set=csv_file.read().decode('UTF-8')
        io_string=io.StringIO(data_set)
        next(io_string)
        for column in csv.reader(io_string,quotechar="|"):
            _,created=DataToBeStored.objects.update_or_create(
            ProductName= column[0],
            ProductPrice=column[1],
            ProductRating=column[2]
            )
        # if form.is_valid():
        #     form.save()
        #     return redirect('file')
    # else:
        # form=DataToBeStoredForm()

    context={}
    return render(request,'file_upload.html',context)

class FileViewset(viewsets.ModelViewSet):
    queryset = DataToBeStored.objects.all()
    serializer_class = DataToBeStoredSerializer
    filter_backends = (SearchFilter,OrderingFilter)
    search_fields=['ProductName']
    # pagination_class =PostLimitOffPagination
    # pagination_class =PostPageNumberPagination

