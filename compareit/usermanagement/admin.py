from django.contrib import admin
from .models import DataToBeStored
from import_export import resources
from  import_export.admin import ImportExportActionModelAdmin

@admin.register(DataToBeStored)
class ViewAdmin(ImportExportActionModelAdmin):
    pass
