from rest_framework.pagination import (
LimitOffsetPagination,
PageNumberPagination
)

class PostLimitOffPagination(LimitOffsetPagination):
    default_limit = 20
    max_limit = 25

class PostPageNumberPagination(PageNumberPagination):
    page_size = 20