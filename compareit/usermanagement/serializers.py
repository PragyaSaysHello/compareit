from rest_framework import serializers
from .models import File, DataToBeStored

class FileSerializer(serializers.ModelSerializer):

    class Meta:
        model=File
        fields="__all__"

class DataToBeStoredSerializer(serializers.ModelSerializer):

    class Meta:
        model=DataToBeStored
        fields="__all__"
