from django.urls import path
from . import views
from rest_framework.routers import DefaultRouter

# urlpatterns=[
#     path('',views.Home.as_view(),name='home'),
#     # path('upload/',views.upload,name='upload'),
#     # path('file/',views.show_file, name='file'),
#     path('uploadfile/',views.upload_file, name='upload_file'),
# ]
router=DefaultRouter()
router.register('',views.FileViewset,basename='files')
urlpatterns=router.urls