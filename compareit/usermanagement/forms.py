from django.forms import ModelForm
from .models import File,DataToBeStored


class FileForm(ModelForm):

    class Meta:
        model= File
        fields= ["name", "filepath"]

class DataToBeStoredForm(ModelForm):

    class Meta:
        model=DataToBeStored
        fields="__all__"