from django.db import models

class DataToBeStored(models.Model):
    ProductName=models.TextField()
    ProductPrice=models.CharField(max_length=100)
    ProductRating=models.CharField(max_length=100)


class File(models.Model):
    name= models.CharField(max_length=500)
    filepath= models.FileField(upload_to='files/', null=True, verbose_name="")



    def __str__(self):
        return self.name + ": " + str(self.filepath)


